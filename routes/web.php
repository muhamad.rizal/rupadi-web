<?php
use RealRashid\SweetAlert\Facades\Alert;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/tes',"PageTes2Controller@index");
// Route::get('/user',"PageUserController@index");
// Route::get('/adduser',"PageUserController@insert");
// Route::get('/profil',"PageProfilsController@index");



Auth::routes();
Route::get('/tes2',function(){
    Alert::error('pesan yang ingin disampaikan', 'Judul Pesan');
	return view("homepage.tes2");
});
Route::get('/', 'PageBerandaController@index');
Route::get('/searchProv/{nama}', 'PageBerandaController@searchProv');
Route::post('/search', 'PageBerandaController@search');
Route::get('/product', 'PageBerandaController@product');
Route::get('/category/{slug}', 'PageBerandaController@productbycategory');
Route::get('/product/detail/{slug}', 'PageBerandaController@detail');
Route::get('/penjual', 'PageBerandaController@penjual');
Route::get('/penjual/{id}','PageBerandaController@productbypenjual');

Route::get('auth/register','PageAuthController@register');
Route::post('auth/register','PageAuthController@store')->name('home.register');
Route::get('verfikasi/register/{token}','PageAuthController@verif');
Route::post('auth/login','PageAuthController@login');
Route::get('logout','PageBerandaController@logout');
Route::get('tes','PageBerandaController@tes');
Route::post('/cart','PageCartController@index');
Route::get('keranjang','PageCartController@keranjang');
Route::post('cart/update','PageCartController@update');
Route::get('cart/delete/{rowid}','PageCartController@delete');
Route::get('cart/formulir','PageCartController@formulir');
Route::post('cart/transaction','PageCartController@transaction');
Route::get('cart/myorder','PageCartController@myorder');
Route::get('cart/detail/{code}','PageCartController@detail');
Route::get('myproduct','PageCartController@product');
Route::get('cart/via/{nama}','PageCartController@via');
// ->middleware('oauth:supplier');
Route::get('addproduct','PageCartController@addproduct');
Route::post('addproduct','PageCartController@saveproduct');
Route::get('editproduct/{id}','PageCartController@editproduct');
Route::post('editproduct','PageCartController@updateproduct');
Route::get('deleteproduct/{id}','PageCartController@deleteproduct');
Route::get('/detailTransaksi','PageCartController@detailTransaksi');
Route::get('myprofil','PageBerandaController@myprofil');
Route::post('updateprofil','PageBerandaController@updateprofil');
Auth::routes();
Route::get('citybyid/{id}',function($id){
	return city($id);
});
Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('admin')->group(function(){
    Route::get('dashboard','PageHomeController@index');
    // Route::get('category','PageCategoryController@index');
    // Route::post('category','PageCategoryController@store');
    Route::resource('category','PageCategoryController');
    Route::get('/category/destroy/{id}','PageCategoryController@destroy');
    Route::resource('product','PageProductController');
});

