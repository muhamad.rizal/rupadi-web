<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    function user(){
        return $this->belongsTo("App\User");
    }
}
