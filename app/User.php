<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;


class User extends Model implements Authenticatable
{
    //
    use AuthenticableTrait;
    protected $fillable = ['name','username','email','password','address','phone','gender','status','birthday','role','remember_token'];

    function product(){
        return $this->hasMany('App\Product');
    }
}
