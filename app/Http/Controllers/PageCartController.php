<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Product;
use Auth;
use App\Category;
use App\User;
use App\Transaction;
use App\Provinsi;
use RealRashid\SweetAlert\Facades\Alert;


class PageCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $category ;
	public function __construct(){
		$this->category = Category::where('parent_id',null)->get();
	}

    public function index(Request $req){
        // Cart::destroy();
    	$product = Product::find($req->id);
    	Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => $req->qty, 'price' => $product->price,'weight' => $product->weight]);
    	return redirect('keranjang');
    }

    public function keranjang(){
    	$category = $this->category;
    	return view('homepage.keranjang',compact('category'));
    }

    public function update(Request $req){
    	Cart::update($req->rowid, $req->qty);
    	$category = $this->category;
    	return redirect('keranjang');
    }

    public function delete($rowid){
    	Cart::remove($rowid);
        $category = $this
        ->category;
    	return redirect('keranjang');
    }

    public function formulir(){
        $category = $this->category;
        return view('homepage.formulir',compact('category'));

    }

    public function transaction(Request $req){
        foreach(Cart::content() as $row){
             $product = Product::find($row->id);
           
             $city = json_decode(City(),true);
             $weight = $product->weight * $row->qty;
             
             foreach ($city['rajaongkir']['results'] as $key ) {
                //  $stok = $product->stock;
                //  $stok = $stok-$row->qty;
                //  $product->stock = $stok;
                //  $product->save();
                 
                 if($product->user->address == $key['city_name']){
                      $cost = Cost($key['city_id'],$req->city,$weight,$req->eks);
                      $data = json_decode($cost,true);
                      Cart::update($row->rowId,['options' => [
                         'code'  => $data['rajaongkir']['results'][0]['code'],
                         'name'  => $data['rajaongkir']['results'][0]['name'],
                         'value' => $data['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value'],
                         'etd'   => $data['rajaongkir']['results'][0]['costs'][0]['cost'][0]['etd']
                         ]]);
                     $eks = [
                        'code' =>  $row->options->code, 
                         'name' =>$row->options->name,
                         'value' => $row->options->value,
                         'etd' =>$row->options->etd
                     ];
                     $transaction = new Transaction;
                     $transaction->code = date('ymdhi').Auth::user()->id;
                     $transaction->user_id = Auth::user()->id;
                     $transaction->qty = $row->qty;
                     $transaction->subtotal = $row->subtotal;
                     $transaction->name = $req->name;
                     $transaction->detail_address = $req->detail_address;
                     $transaction->address = $req->city;
                     $transaction->portal_code = $req->portal_code;
                     $transaction->ekspedisi = $eks;
                     $transaction->supplier_id = $product->user_id;
                     $transaction->product_id = $product->id;
                     $transaction->save();
                     $LastInsertId = $transaction->id;
                     
                     Cart::remove($row->rowId);
                 }   
             }
             if(Cart::count() == 0)
             {
                $category = $this->category;
                session()->put('id',$LastInsertId);
                Alert::success('RUPADI.COM', 'Pilih Metode Pembayaran');   
                return view("homepage.modePembayaran",compact("category"));
             }
         }
     }

     public function myorder(){
        $category = $this->category;
        $transaction = Transaction::where('user_id',Auth::user()->id)->get();

        return view('homepage.myorder',compact('category','transaction'));
    }

    public function detail($code){
        $transaction = Transaction::where('code',$code)->first();
		$transactiondetail = Transaction::orderBy('id','DESC')->where('code',$code)->get();
        $category = $this->category;
        return view('homepage.detailtransaksi',compact('category','transaction','transactiondetail'));
    }
    public function product(){
        $category = $this->category;
        $product = Product::where('user_id',Auth::user()->id)->get();
        return view('homepage.myproduct',compact('product','category'));
    }
    public function addproduct(){
        $category = $this->category;
        $provinsi = Provinsi::all();
        // $mycategorys = Category::where('parent_id',null)->get();
        return view('homepage.addproduct',compact('category','provinsi'));
    }
    public function saveproduct(Request $request){
        
        $file = $request->file('file');
        $filename = $file->getClientOriginalName();
        $request->file('file')->move('static/dist/img/',$filename);
        $product = new Product;
        $product->slug = $request->slug;
        $product->photo = 'static/dist/img/'.$filename;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->stock = $request->stock;
        $product->price = $request->price;
        $product->provinsi = $request->provinsi;
        $product->category_id = $request->category_id;
        $product->weight = $request->weight;
        $product->user_id = Auth::user()->id;
        $product->save();
        Alert::success('', 'Product Berhasil di Tambahkan');
        return redirect('myproduct');

    }

    public function editproduct($id){
        $category = $this->category;
        $product = Product::find($id);
        $provinsi = Provinsi::all();
        // $categorys = Category::where('parent_id',null)->get();
        return view('homepage.editproduct',compact('product','category','provinsi'));
    }
    public function updateproduct(Request $request){
        $id = $request->id;
        if( $file = $request->file('file'))
        {
            $filename = $file->getClientOriginalName();
            $request->file('file')->move('static/dist/img/',$filename);
            $img = 'static/dist/img/'.$filename;
        }else
        {
            $img = $request->tmp_image ;
        }
        $product = Product::find($id);
        $product->slug = $request->slug;
        $product->photo = $img;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->stock = $request->stock;
        $product->price = $request->price ;
        $product->provinsi = $request->provinsi ;
        $product->category_id = $request->category_id;
        $product->user_id = Auth::user()->id;
         $product->weight = $request->weight;
        $product->save();

        // Alert::success('', 'Product Berhasil di Update');
        Alert::success('RUPADI.COM', 'Product Berhasil di Update');
        return redirect('myproduct');
    }
    public function deleteproduct($id){
        $product = Product::find($id);
        $product->delete();
        // Alert::success('','Product Berhasil di delete');
        Alert::success('RUPADI.COM', 'Product Berhasil di delete');
        return redirect('myproduct');
    }
    
    public function detailTransaksi(){
        $category = $this->category;
        $transaksi=Transaction::orderBy('id','desc')->where('supplier_id',Auth::user()->id)->get();
        // Alert::success('','Product Berhasil di delete');
        return view('homepage.dTransaksi',compact('transaksi','category'));
    }

    function via($nama){
        


        switch ($nama) {
            case 'bca':

                $id_bank = 1;
                $virtual = "BCA";
                    
            break;
            
            case 'mandiri':

                $id_bank = 1;
                $virtual = "MANDIRI";
                    
            break;
            
            case 'alfamart':
                $id_bank = 1;
                $virtual = "ALFAMART";
                    
            break;
            
                case 'indomaret':
                    $id_bank = 1;
                    $virtual = "INDOMARET";
                
                    break;
        }

        $transaction = new Transaction;
        $transaction = $transaction::where('id',session()->get("id"))->first();
        $produk = Product::find($transaction->product_id);
        $stock = $produk->stock;
        $stock = $stock - $transaction->qty;
        $produk->stock = $stock;
        $produk->save();

        $transaction->via = $virtual;
        $transaction->save();
        session()->forget('id');
        return redirect("/cart/myorder");
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
