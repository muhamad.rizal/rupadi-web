<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class PageCategoryController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $categorys = Category::where('parent_id',null)->get();
        return view('admin.category.index',compact('categorys'));
    }

    public function store(Request $request){
        $category = new Category;
        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->parent_id = $request->parent_id;
        $category->save();
        
        return redirect("admin/category");
    }

    public function edit($category){
        
        $categorys = Category::find($category);
        $category_parent = Category::where('parent_id',null)->get();
        return view('admin.category.edit',compact('categorys','category_parent'));
    }

    public function update(Request $request, $id){
        $categorys = Category::find($id);
        $categorys->name = $request->name;
        $categorys->slug = $request->slug;
        $categorys->parent_id = $request->parent_id;
        $categorys->save();

        return redirect("admin/category");

    }

    public function destroy($id){
        $category = Category::find($id);
        $category->delete();
        return redirect("admin/category");
    }

}
