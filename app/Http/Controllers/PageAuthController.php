<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\User;
use Mail;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;


use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
// use Alert;

class PageAuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $category ;
	public function __construct(){
		$this->category = Category::where('parent_id',null)->get();
    }
    
    public function register()
    {

        $category = $this->category;
    	return view('homepage.register',compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $data)
    {
        $remember_token = base64_encode($data['email']);
        $mydata =  [
            'name' => $data['name'],
            'email' => $data['email'],
            'username'  => $data['username'],
            'address'   => $data['address'],
            'phone'     => $data['phone'],
            'gender'    => $data['gender'],
            'birthday'  => $data['birthday'],
            'role'      => $data['role'],
            'status'    => "0",
            'password' => bcrypt($data['password']),
            'remember_token' => $remember_token,
        ];
		User::create($mydata);
        Mail::send('home', array('firstname' => $data['name'],'remember_token' => $remember_token) , function($pesan) use($data){
            $pesan->to($data['email'],'Verifikasi')->subject('Verifikasi Email');
            $pesan->from('pamungkasray229@gmail.com','Verifikasi Akun email anda');
        });
        Alert::success('RUPADI.COM', 'Pendaftaran selesai, Silahkan Cek Email Anda Untuk Verivikasi');
   		return redirect('/');
    }

    public function verif($token){
        $user = User::where('remember_token',$token)->first();
        if($user->status=="0"){
            $user->status = "1";
        }
        $user->save();
        // Alert::success('', 'Verifikasi Sukses,silahkan login');
        Alert::success('RUPADI.COM', 'Verivikasi sukses,silahkan login');
        return redirect('auth/register');
    }
    public function login(Request $request){
        $email =  $request->email;
        $pwd   =  $request->password;

        if(Auth::attempt(['email' => $email,'password' => $pwd])){
            $cek = User::where('id',Auth::user()->id)->first();
            if($cek->status == 0){
                Auth::logout();
                // Alert::success('', 'Maaf akun belum terverifikasi');
                Alert::error('RUPADI.COM', 'Maaf akun belum terverifikasi');
                return redirect('/');
            }else{
                return redirect()->back();
            }
        }else{
            // Alert::success('', 'Maaf Email atau password tidak sesuai');
            Alert::error('RUPADI.COM', 'Maaf Email atau password tidak sesuai');
                return redirect('/');
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
