<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\User;
use App\Provinsi;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;

class PageBerandaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $category ;
	public function __construct(){
		$this->category = Category::where('parent_id',null)->get();
	}



    public function tes(){
        $category = $this->category;
        $products = Product::take(12)->orderBy('id','DESC')->get();
        return view('homepage.modePembayaran',compact('products','category'));
    }

    public function index()
    {
        $category = $this->category;
           $products = Product::take(12)->orderBy('id','DESC')->get();
           $provinsi = Provinsi::all();
        //    Alert::success('Selamat Datang', 'RUPADI.COM');
        return view('homepage.homepage',compact('products','category','provinsi'));
    }

    public function product(){
        $category = $this->category;
        $products = Product::orderBy('id','DESC')->paginate(8);
        return view('homepage.product',compact('products','category'));
    }

    public  function searchProv($nama){
        $products = Product::where("provinsi",$nama)->paginate(8);
        $nama2 = $nama;
        $category = $this->category;
        return view('homepage.search',compact('products','category','nama2'));
    }
    public  function search(Request $request){
        $products = Product::where("name",'like',"%".$request->cari."%")->paginate(8);
        $nama2 = $request->cari;
        $category = $this->category;
        return view('homepage.search',compact('products','category','nama2'));
    } 

    public function productbycategory($slug){
        $categorys = Category::where('slug',$slug)->first();
        $id 	  = $categorys->id;
        $category = $this->category;
        $name 	  = $categorys->name;
        $products = Product::orderBy('id','DESC')->where('category_id',$id)->get();
        return view('homepage.productbycategory',compact('products','category','name'));
}

public function detail($slug){
    $products = Product::where('slug',$slug)->first();
    $category = $this->category;
    return view('homepage.detail',compact('products','category'));
}

public function penjual(){
    $category = $this->category;
    $user = User::orderBy('id','DESC')->where('status',"1")->where('role','=','supplier')->get();
    return view('homepage.supplier',compact('category','user'));
 }

 public function productbypenjual($id){
    $category = $this->category;
    $user = User::find($id);
    $products = $user->product;
    return view('homepage.productbypenjual',compact('products','category','user'));
 }

 public function logout(){
    Auth::logout();
    Alert::success('RUPADI.COM', 'Berhasil Logout');
    return redirect('/');
 }

 public function myprofil(){
    $category = $this->category;
    $user  = User::where('id',Auth::user()->id)->first();
    return view('homepage.myprofil',compact('category','user'));
 }

 public function updateprofil(Request $data){
    if( $file = $data->file('file'))
        {
            $filename = $file->getClientOriginalName();
            $data->file('file')->move('static/dist/img/',$filename);
            $img = 'static/dist/img/'.$filename;
        }else
        {
            $img = $data->tmp_image ;
        }
    $mydata = ([
        'name' => $data['name'],
        'email' => $data['email'],
        'username'  => $data['username'],
        'address'   => $data['address'],
        'phone'     => $data['phone'],
        'gender'    => $data['gender'],
        'birthday'  => $data['birthday'],
        'role'      => $data['role'],
        'status'    => "0",
        'photo'     => $img,
        'password' => bcrypt($data['password']),
    ]);
    User::where('id',$data->id)->update($mydata);
    // Alert::success('', 'Profil  berhasil di perbarui');
    Alert::success('RUPADI.COM', 'Profil  berhasil di perbarui');
    
    return redirect('myprofil');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
