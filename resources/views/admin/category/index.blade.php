@extends('admin.layout.master')
@section('header')
<link rel="stylesheet" href="{{ asset('static/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('body')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Blank Page</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Blank Page</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->

<div class="container-fluid">

  <div class="row">

    <div class="col">

        <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Quick Example</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/admin/category" method="POST">
              @csrf
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama</label>
                  <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Nama Category">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Slug</label>
                    <input type="text" name="slug" class="form-control" id="exampleInputEmail1" placeholder="Nama Slug">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Parent Category</label>
                    <select name="parent_id" class="form-control" id="exampleInputEmail1">
                      <option value="">Buat Master Category Baru</option>  
                      @foreach ($categorys as $data)                        
                         <option value="{{$data->id}}">{{$data->name}}</option>
                        @endforeach
                    </select>
                  </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>

    </div>

    <div class="col">

          <section class="content">
        
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">DataTable with default features</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th style="width:50px !important">No</th>
                      <th >Category</th>
                      <th >Sub Category</th>
                    </tr>
                    </thead>
                    <tbody>
             
                        @php
                        $no_id = 1;
                    @endphp
                    
                    @foreach ($categorys as $data)
                    <tr>
                        <td>{{$no_id++}}</td>
                        <td>
                          {{ $data->name }} <a href="/admin/category/{{$data->id}}/edit"><i class="fas fa-edit"></i> Edit </a> | 
                          <a href="/admin/category/destroy/{{$data->id}}" onclick="return confirm('Apakah Anda Yakin ?');"><i class="fas fa-trash"></i> Hapus</a>
                        </td>
                        <td>
                            <ul>
                                @foreach ($data->children as $data2)
                                    
                            <li>{{$data2->name}} <a href="/admin/category/{{$data2->id}}/edit"> <i class="fas fa-edit"></i> edit</a> <a href="/admin/category/destroy/{{$data2->id}}" onclick="return confirm('Apakah Anda Yakin ?');"> <i class="fas fa-trash"></i> Hapus</a>    </li> 
                                @endforeach
                            </ul>
                        </td>



                      </tr>
                    


                    @endforeach
        
        
                    </tbody>
                    <tfoot>
             
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
        
          </section>
      </div>

  </div>

</div>
  
@endsection

@section('footer')
  <!-- DataTables -->
<script src="{{ asset('static/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('static/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('static/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
    });
  });
</script>
@endsection



@show