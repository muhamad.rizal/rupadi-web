@extends('admin.layout.master')
@section('header')
<link rel="stylesheet" href="{{ asset('static/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('body')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Blank Page</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Blank Page</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->

<div class="container-fluid">

  <div class="row justify-content-center">

    <div class="col-md-5">

        <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Quick Example</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/admin/category/{{$categorys->id}}" method="POST">
              @csrf
              @method('PUT') 

              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Category</label>
                <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Nama Category" value="{{$categorys->name}}">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Slug</label>
                    <input type="text" name="slug" class="form-control" id="exampleInputEmail1" placeholder="Nama Slug" value="{{$categorys->slug}}">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Icon</label>
                    <input type="text" name="icon" class="form-control" id="exampleInputEmail1" placeholder="Nama icon" value="{{$categorys->icon}}">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Parent Category</label>
                    <select name="parent_id" class="form-control" id="exampleInputEmail1">
                        @if ($categorys->parent_id == null)
                        <option value="null">Menjadi Parent</option>
                        @else
                        
                            @foreach ($category_parent as $data)                        
                            <option value="{{$data->id}}" @if ($categorys->parent_id == $data->id)
                                selected
                            @endif >{{$data->name}}</option>
                            @endforeach  
                            
                        @endif

                    </select>
                  </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>

    </div>


  </div>

</div>
  
@endsection

@section('footer')
  <!-- DataTables -->
<script src="{{ asset('static/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('static/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('static/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
    });
  });
</script>
@endsection



@show