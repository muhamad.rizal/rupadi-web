@extends('admin.layout.master')
@section('header')
<link rel="stylesheet" href="{{ asset('static/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('body')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Blank Page</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Blank Page</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->

<div class="container-fluid">

  <div class="row">

    <div class="col">

          <section class="content">
        
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">DataTable with default features</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th style="width:50px !important">No</th>
                      <th >Photo</th>
                      <th >Product</th>
                      <th style="width:400px !important">Deskripsi</th>
                      <th >Stock</th>
                      <th>Price</th>
                      <th >Action</th>
                    </tr>
                    </thead>
                    <tbody>
             
                        @php
                        $no_id = 1;
                    @endphp
                    
                    @foreach ($product as $data)
                    <tr>
                        <td>{{$no_id++}}</td>
                        <td>
                            <img src="{{ $data->photo }}" width="100px" alt="">
                           
                        </td>
                        <td>
                            {{ $data->name }}
                        </td>
                        <td>
                            {{ $data->description }}
                        </td>
                        <td>
                            {{ $data->stock }}
                        </td>
                        <td>
                            {{ $data->price }}
                        </td>
                        <td>
                            <a class="btn btn-warning" href="/admin/product/{{$data->id}}/edit"> <i class="fas fa-edit"></i> edit</a> <a class="btn btn-danger" href="/admin/product/destroy/{{$data->id}}" onclick="return confirm('Apakah Anda Yakin ?');"> <i class="fas fa-trash"></i> Hapus</a>
                        </td>
                        <td>
                            {{ $data->category_id }}
                        </td>
                        <td>
                            {{ $data->user_id }}
                        </td>
                        



                      </tr>
                    


                    @endforeach
        
        
                    </tbody>
                    <tfoot>
             
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
        
          </section>
      </div>

  </div>

</div>
  
@endsection

@section('footer')
  <!-- DataTables -->
<script src="{{ asset('static/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('static/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('static/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
    });
  });
</script>
@endsection



@show