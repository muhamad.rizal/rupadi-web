@extends('homepage.index')
@section('header')
    <title>RUPADI.COM</title>

@endsection
@section('slide')
  @include('homepage.layout.slider')
@endsection
@section('contents')

<div class="row">
  @include('sweet::alert')
<div class="col-md-9">
  <div id="content">
        <div class="container">
          <div class="row bar">
            <div class="col-md-12">
              <div class="text-center panel-heading">
              <h3 class="text-center panel-title">Jual Produk Kesenian terlengkap.</h3>
              </div>
              <div class="products-big">
                <div class="row products">
                  @foreach($products as $product)
                  <div class="col-lg-3 col-md-4">
                    <div class="product">
                      <div class="image">
                        <a href="{{ url('product/detail/'.$product->slug) }}"><img src="{{ url($product->photo) }}" alt="" class="img-fluid image1"></a></div>
                      <div class="text">
                        <h3 class="h5"><a href="{{ url('product/detail/'.$product->slug) }}">
                          {{ $product->name }}
                        </a></h3>
                        <p>Provinsi : {{ $product->provinsi }}</p>
                        <p class="price">Rp. {{ number_format($product->price) }}</p>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
              <div class="pages">
                <p class="loadMore text-center"><a href="{{ url('product') }}" class="btn btn-template-outlined"><i class="fa fa-chevron-down"></i> Load more</a></p>
              </div>
            </div>
          </div>
        </div>
  </div>
</div>

  <div class="col-md-3 mt-5">
    <!-- MENUS AND FILTERS-->
    <div class="panel panel-default sidebar-menu">
      <div class="panel-heading">
        <h3 class="h4 panel-title">Provinsi</h3>
      </div>
      <div class="panel-body">
        <ul class="nav nav-pills flex-column text-sm category-menu">
          @foreach ($provinsi as $prov)
        <li class="nav-item"><a href="searchProv/{{$prov->nama}}" class="nav-link d-flex align-items-center justify-content-between"><span>{{$prov->nama}}</span><span class="badge badge-secondary">42</span></a>
              
          @endforeach
        </ul>
      </div>
    </div>
    
</div>
</div>
</div>
</div>
<!-- GET IT-->

<!-- FOOTER -->
<footer class="main-footer">
<div class="container">
<div class="row">
  
  
@endsection

@section('footer')

@endsection
@show