@extends('homepage.index')
@section('header')
<title>RUPADI.COM</title>

@endsection
@section('slide')

@endsection
@section('contents')
  <div id="content">
        <div class="container">
          <div class="row bar">
            <div class="col-md-12">
              <p class="text-muted lead text-center">Jual Komputer dan Gedget terlengkap.</p>
              <div class="products-big">
                <div class="row products">
                  <div class="col-lg-3 col-md-4">
                    <div class="product">
                      <div class="image">
                        <a href="via/bca"><img src="https://statik.tempo.co/data/2019/04/23/id_836405/836405_720.jpg" alt="" class="img-fluid image1"></a></div>
                      <div class="text">
                        <h3 class="h5"><a href="via/bca">
                            BCA
                        </a></h3>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-4">
                    <div class="product">
                      <div class="image">
                        <a href="via/mandiri"><img src="https://rec-data.kalibrr.com/logos/B6XQQBTZRVSSRRDGSFYFEMDYWYZU697HY3PPCEYE-5a4da1cd.jpg" alt="" class="img-fluid image1"></a></div>
                      <div class="text">
                        <h3 class="h5"><a href="via/mandiri">
                            Mandiri
                        </a></h3>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-4">
                    <div class="product">
                      <div class="image">
                        <a href="via/alfamart/"><img src="https://www.alfamartku.com/apps/addons/shared_addons/themes/alfamartku/img/alfamartku.jpg" alt="" class="img-fluid image1"></a></div>
                      <div class="text">
                        <h3 class="h5"><a href="via/alfamart">
                            Alfamart
                        </a></h3>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-4">
                    <div class="product">
                      <div class="image">
                        <a href="via/indomaret"><img src="https://upload.wikimedia.org/wikipedia/id/thumb/2/28/Indomaret.png/1200px-Indomaret.png" alt="" class="img-fluid image1"></a></div>
                      <div class="text">
                        <h3 class="h5"><a href="via/indomaret">
                            Indomaret
                        </a></h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="pages">
                <p class="loadMore text-center"><a href="{{ url('product') }}" class="btn btn-template-outlined"><i class="fa fa-chevron-down"></i> Load more</a></p>
              </div>
            </div>
          </div>
        </div>
  </div>

@endsection

@section('footer')

@endsection
@show