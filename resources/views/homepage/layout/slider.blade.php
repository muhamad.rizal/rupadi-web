<section style="background: url('img/photogrid.jpg') center center repeat; background-size: cover;" class="bar background-white relative-positioned">
    <div class="container">
      <!-- Carousel Start-->
      <div class="home-carousel">
        <div class="dark-mask mask-primary"></div>
        <div class="container">
          <div class="homepage owl-carousel">
            <div class="item">
              <div class="row">
                <div class="col-md-10 text-right">
                  <p><img src="{{ url('homepage/img/logo.png')}}" alt="" class="ml-auto"></p>
                  <h1>Memiliki Persedian Barang yang lengkap</h1>
                  <p>Rupadi memiliki berbagai macam persediaan Produk Kesenian</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Carousel End-->
    </div>
  </section>