-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2020 at 02:30 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_budaya`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'fa fa-home',
  `name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `slug`, `icon`, `name`, `parent_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Fashion', '', 'Fashion', NULL, 1, NULL, NULL),
(2, 'Alat Musik', '', 'Alat Musik', NULL, 1, NULL, NULL),
(9, 'Mainan', 'fa fa-home', 'Mainan', NULL, NULL, '2020-02-08 08:47:07', '2020-02-08 08:47:07'),
(12, 'Makanan', 'fa fa-home', 'Makanan', NULL, NULL, '2020-02-13 09:24:54', '2020-02-13 09:24:54'),
(14, 'kesenian', 'fa fa-home', 'kesenian', NULL, NULL, '2020-02-28 09:30:56', '2020-02-28 09:30:56');

-- --------------------------------------------------------

--
-- Table structure for table `keluhans`
--

CREATE TABLE `keluhans` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `pesan` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `keluhans`
--

INSERT INTO `keluhans` (`id`, `nama`, `email`, `phone`, `pesan`, `created_at`, `updated_at`) VALUES
(1, 'pamungkas', 'if18.raypamungkas@mhs.ubpkarawang.ac.id', '083815854063', 'tes', '2020-02-28 19:11:46', '2020-02-28 19:11:46'),
(2, 'pamungkas', 'if18.raypamungkas@mhs.ubpkarawang.ac.id', '083815854063', 'tes2', '2020-02-28 19:12:21', '2020-02-28 19:12:21');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(12, '2020_02_06_201631_create_table_users', 1),
(13, '2020_02_07_004858_create_password_resets_table', 1),
(14, '2020_02_07_212145_table_category', 1),
(15, '2020_02_07_212429_table_products', 1),
(16, '2020_02_07_212600_table_transaction', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` int(11) NOT NULL DEFAULT 1,
  `stock` int(11) NOT NULL,
  `terjual` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `rating` float DEFAULT NULL,
  `banyak_rating` int(11) DEFAULT NULL,
  `provinsi` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `photo`, `name`, `slug`, `description`, `weight`, `stock`, `terjual`, `price`, `rating`, `banyak_rating`, `provinsi`, `category_id`, `user_id`, `created_at`, `updated_at`) VALUES
(16, 'static/dist/img/Angklung-Sarinande-8.jpg', 'angklung sunda', 'angklungsunda', 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsumlorem ipsumlorem ipsum lorem ipsum lorem ipsumlorem ipsum lorem ipsum', 1, 86, 7, 50000, 12, 3, 'Jawa Barat', 2, 13, '2020-02-26 22:04:45', '2020-02-28 16:52:35'),
(17, 'static/dist/img/congklak.jpg', 'congklak jawa barat', 'congklak', 'congklak jawa barat', 2, 91, 16, 50000, 4, 1, 'Jawa Barat', 9, 2, '2020-02-28 08:37:27', '2020-02-28 21:38:42'),
(18, 'static/dist/img/ee4deba50effc5790fa50b55b41a538a.jpg', 'baju batik khas karawang', 'bajubatik', 'baju batik khas karawang', 1, 95, 5, 100000, 5, 1, 'Jawa Barat', 1, 2, '2020-02-28 08:38:37', '2020-02-28 21:31:16'),
(20, 'static/dist/img/wayang-golek-300x143.jpg', 'Wayang Golek', 'WayangGolek', 'Wayang Golek', 1, 50, NULL, 200000, NULL, NULL, 'Jawa Barat', 14, 2, '2020-02-28 21:43:39', '2020-02-28 21:43:39'),
(21, 'static/dist/img/geundrang alat musik tradisional aceh.gif', 'geundrang', 'geundrang', 'alat musik provinsi aceh', 5, 17, 3, 250000, 5, 1, 'Aceh', 2, 2, '2020-02-28 21:48:53', '2020-02-29 06:52:19'),
(22, 'static/dist/img/kujang.jpg', 'Kujang', 'Kujang', 'Kujang merupakan senjata tradisional Jawa Barat', 2, 20, NULL, 365000, NULL, NULL, 'Jawa Barat', 14, 2, '2020-02-29 07:13:24', '2020-02-29 07:13:24'),
(23, 'static/dist/img/batik pekalongan.jpg', 'Baju Batik Pekalongan Pria', 'Batik Pekalongan', 'Bahan nyaman\r\nTersedia ukuran L, M & XL', 1, 80, NULL, 120000, NULL, NULL, 'Jawa Tengah', 1, 2, '2020-02-29 07:18:29', '2020-02-29 07:18:29'),
(24, 'static/dist/img/ondel ondel.jpg', 'Boneka Ondel-ondel', 'Ondel-ondel', 'Boneka ondel-ondel cocok untuk mainan anak', 1, 150, NULL, 35000, NULL, NULL, 'DKI Jakarta', 9, 2, '2020-02-29 07:21:34', '2020-02-29 07:21:34'),
(25, 'static/dist/img/kerambit.jpg', 'Kerambit', 'Kerambit', 'Kerambit merupakan salah satu senjata tradisional Sumatera Barat', 1, 10, NULL, 115000, NULL, NULL, 'Sumatera Barat', 14, 2, '2020-02-29 07:25:04', '2020-02-29 07:25:04'),
(26, 'static/dist/img/Keledik-Kedire-Kadire.JPG', 'kedire', 'Kedire', 'Keledik/Kedire merupakan alat musik terbuat dari labu dan bilah bambu di mainkan dengan cara ditiup dan dihisap', 1, 75, NULL, 200000, NULL, NULL, 'Kalimantan Timur', 2, 2, '2020-02-29 07:28:59', '2020-02-29 07:28:59'),
(27, 'static/dist/img/kain ulos.jpg', 'Kain Ulos', 'kain ulos', 'Jual macam-macam kain ulos khas sumatera utara', 1, 1000, NULL, 850000, NULL, NULL, 'Sumatera Utara', 1, 2, '2020-02-29 07:35:33', '2020-02-29 07:35:33');

-- --------------------------------------------------------

--
-- Table structure for table `provinsis`
--

CREATE TABLE `provinsis` (
  `id` char(2) NOT NULL,
  `nama` tinytext DEFAULT NULL,
  `ibu_kota` varchar(255) DEFAULT NULL,
  `nama_gubernur` varchar(100) DEFAULT NULL,
  `luas` varchar(100) DEFAULT NULL,
  `hari_jadi` date DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `gambar1` varchar(255) DEFAULT NULL,
  `keterangan1` varchar(255) DEFAULT NULL,
  `gambar2` varchar(255) DEFAULT NULL,
  `keterangan2` varchar(255) DEFAULT NULL,
  `gambar3` varchar(255) DEFAULT NULL,
  `keterangan3` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `provinsis`
--

INSERT INTO `provinsis` (`id`, `nama`, `ibu_kota`, `nama_gubernur`, `luas`, `hari_jadi`, `deskripsi`, `website`, `icon`, `gambar1`, `keterangan1`, `gambar2`, `keterangan2`, `gambar3`, `keterangan3`, `created_at`, `updated_at`) VALUES
('11', 'Aceh', 'Banda Aceh', 'Nova Iriansyah', '57,956 km2 (22,377 sq mi)', '2014-07-04', 'Aceh (abjad Jawoë: اچيه دارالسلام) adalah sebuah provinsi di Indonesia yang ibu kotanya berada di Banda Aceh. Aceh merupakan salah satu provinsi di Indonesia yang diberi status sebagai daerah istimewa dan juga diberi kewenangan otonomi khusus. Aceh terletak di ujung utara pulau Sumatra dan merupakan provinsi paling barat di Indonesia. Menurut hasil sensus Badan Pusat Statistik tahun 2019, jumlah penduduk provinsi ini sekitar 5.281.891Jiwa.[10] Letaknya dekat dengan Kepulauan Andaman dan Nikobar di India dan terpisahkan oleh Laut Andaman. Aceh berbatasan dengan Teluk Benggala di sebelah utara, Samudra Hindia di sebelah barat, Selat Malaka di sebelah timur, dan Sumatra Utara di sebelah tenggara dan selatan.\r\n\r\nAceh dianggap sebagai tempat dimulainya penyebaran Islam di Indonesia dan memainkan peran penting dalam penyebaran Islam di Asia Tenggara. Pada awal abad ke-17, Kesultanan Aceh adalah negara terkaya, terkuat, dan termakmur di kawasan Selat Malaka. Sejarah Aceh diwarnai oleh kebebasan politik dan penolakan keras terhadap kendali orang asing, termasuk bekas penjajah Belanda dan pemerintah Indonesia. Jika dibandingkan dengan dengan provinsi lainnya, Aceh adalah wilayah yang sangat konservatif (menjunjung tinggi nilai agama).[11] Persentase penduduk Muslimnya adalah yang tertinggi di Indonesia dan mereka hidup sesuai syariah Islam.[12] Berbeda dengan kebanyakan provinsi lain di Indonesia, Aceh memiliki otonomi yang diatur tersendiri karena alasan sejarah.[13]', 'https://www.acehprov.go.id/', '/static/dist/img/1200px-Coat_of_arms_of_Aceh.png', '/static/dist/img/download.jpg', 'tari aceh', '/static/dist/img/download (2).jpg', 'alat musik aceh', '/static/dist/img/download (1).jpg', 'baju adat', '0000-00-00 00:00:00', '2020-02-28 23:45:33'),
('12', 'Sumatera Utara', 'Medan', 'Edy Rahmayadi', '72.981,23 km²', '1948-12-04', 'Provinsi Sumatra Utara terletak pada 1° - 4° Lintang Utara dan 98° - 100° Bujur Timur, Luas daratan Provinsi Sumatra Utara 72.981,23 km².\r\n\r\nSumatra Utara pada dasarnya dapat dibagi atas:\r\n\r\nPesisir Timur\r\nPegunungan Bukit Barisan\r\nPesisir Barat\r\nKepulauan Nias\r\nPesisir timur merupakan wilayah di dalam provinsi yang paling pesat perkembangannya karena persyaratan infrastruktur yang relatif lebih lengkap daripada wilayah lainnya. Wilayah pesisir timur juga merupakan wilayah yang relatif padat konsentrasi penduduknya dibandingkan wilayah lainnya. Pada masa kolonial Hindia Belanda, wilayah ini termasuk residentie Sumatra\'s Oostkust bersama provinsi Riau.\r\n\r\nDi wilayah tengah provinsi berjajar Pegunungan Bukit Barisan. Di pegunungan ini terdapat beberapa wilayah yang menjadi kantong-kantong konsentrasi penduduk. Daerah di sekitar Danau Toba dan Pulau Samosir, merupakan daerah padat penduduk yang menggantungkan hidupnya kepada danau ini.\r\n\r\nPesisir barat merupakan wilayah yang cukup sempit, dengan komposisi penduduk yang terdiri dari masyarakat Batak, Minangkabau, dan Aceh. Namun secara kultur dan etnolinguistik, wilayah ini masuk ke dalam budaya dan Bahasa Minangkabau.[6]\r\n\r\nBatas wilayah\r\nUtara	Provinsi Aceh dan Selat Malaka\r\nTimur	Selat Malaka\r\nSelatan	Provinsi Riau, Provinsi Sumatra Barat, dan Samudera Indonesia\r\nBarat	Provinsi Aceh dan Samudera Indonesia', 'http://www.sumutprov.go.id', '/static/dist/img/sumut.png', '/static/dist/img/serampang.jpg', 'Tari Serampang', '/static/dist/img/download (3).jpg', 'Alat Musik Tradisional Provinsi Sumatera Utara (Sumut) terdiri dari: Doli-Doli, Druni Dana, Faritia, Garantung, Gendang Sisibah, Gordang, Hapetan, Hesek, Sulim, Sarune Bolon, Ole-Ole, Odap, Ogung, Pangora, Taganing', '/static/dist/img/baju adat touba.jpg', 'Baju Adat Toba', '0000-00-00 00:00:00', '2020-02-29 14:47:48'),
('13', 'Sumatera Barat', 'Padang', 'Irwan Prayitno', '42.297,30 km²', '1945-01-10', 'Sumatra Barat adalah sebuah provinsi di Indonesia yang terletak di Pulau Sumatra dengan Padang sebagai ibu kotanya. Provinsi Sumatra Barat terletak sepanjang pesisir barat Sumatra bagian tengah, dataran tinggi Bukit Barisan di sebelah timur, dan sejumlah pulau di lepas pantainya seperti Kepulauan Mentawai', 'http://www.sumbarprov.go.id', '/static/dist/img/sumbar.png', '/static/dist/img//static/dist/img/tari-piring.jpg', 'Tari Piring', '/static/dist/img//static/dist/img/download (4).jpg', 'Bansi, Dulang atau Talam, Gandang Tabuik, Kateuba, Kecapi Jepang, Pupuik Batang Padi, Pupuik Tanduak, Rabab, Rebab Pasisie, Saluang, Saluang PauhSerunai, Talempong, Talempong Batu Talang Anau, Tambua & Tansa', '/static/dist/img//static/dist/img/Pakaian-Adat-Minangkabau-pernikahan.jpg', 'baju adat pernikahan', '0000-00-00 00:00:00', '2020-02-29 15:15:47'),
('14', 'Riau', 'Pekanbaru', 'Syamsuar', '87.023,66 km2', '1957-09-08', 'Riau adalah sebuah provinsi di Indonesia yang terletak di bagian tengah pulau Sumatra. Provinsi ini terletak di bagian tengah pantai timur Pulau Sumatra, yaitu di sepanjang pesisir Selat Melaka', 'http://www.riau.go.id', '/static/dist/img/Coat_of_arms_of_Riau.svg.png', '/static/dist/img/tari-malemang.jpg', 'Tari Melemang', '/static/dist/img/rubibig.jpg', 'Rebana Ubi', '/static/dist/img/sewa-baju-adat-pengantin-riau-952dd378bfac-GjR16f-1-1544859492464.jpg', 'Baju Kurung Cekak Musang dan kebaya laboh', '0000-00-00 00:00:00', '2020-02-29 15:27:39'),
('15', 'Jambi', 'Kota Jambi', 'Fachori Umar', '50.058 km²', '1958-06-25', 'Jambi adalah sebuah Provinsi Indonesia yang terletak di pesisir timur di bagian tengah Pulau Sumatra. Jambi adalah nama provinsi di Indonesia yang ibu kotanya bernama sama dengan nama provinsinya, selain Bengkulu, Daerah Khusus Ibukota Jakarta, dan Gorontalo', 'http://www.jambiprov.go.id', '/static/dist/img/150px-Coat_of_arms_of_Jambi.svg.png', '/static/dist/img/tari-sekapur-sirih.png', 'Tarian Sekapur Sirih', '/static/dist/img/Gendang-Melayu.jpg', 'Gendang Melayu', '/static/dist/img/download (5).jpg', 'Melayu Jambi', '0000-00-00 00:00:00', '2020-02-29 15:41:36'),
('16', 'Sumatera Selatan', 'Palembang', 'Herman Deru', '91.592,43 km2', NULL, 'Sumatra Selatan adalah provinsi di Indonesia yang terletak di bagian selatan Pulau Sumatra. Provinsi ini beribu kota di Palembang. Secara geografis, Sumatra Selatan berbatasan dengan provinsi Jambi di utara, provinsi Kep. Bangka-Belitung di timur, provinsi Lampung di selatan dan Provinsi Bengkulu di barat', 'http://www.sumselprov.go.id', '/static/dist/img/sumsel.png', '/static/dist/img/1__Tari_Gending_Sriwijaya_merupakan_tarian_kolosal_peninggalan_Kerajaan_Sriwijaya.jpg', 'Tari Gending Sriwijaya', '/static/dist/img/0c041-burdah2.jpg', 'Burdah', '/static/dist/img/50646833_615053965618911_9004390284132464887_n.jpg', 'Pakaian Aesan gede', '0000-00-00 00:00:00', '2020-02-29 15:54:05'),
('17', 'Bengkulu', 'Bengkulu', 'Rohidin Mersyah', '19.919 km²', '1968-11-18', 'Bengkulu adalah sebuah provinsi di Indonesia. Ibu kotanya berada di Kota Bengkulu. Provinsi ini terletak di bagian barat daya Pulau Sumatra', 'http://www.bengkuluprov.go.id', '/static/dist/img/bengkulu.png', '/static/dist/img/Tari-Kejei.jpg', 'Tari Kejey', '/static/dist/img/Kulintang-Bengkulu.jpg', 'Kulintang Bengkulu', '/static/dist/img/bengkulu-1.jpg', 'Pakaian Pengantin Bengkulu', '0000-00-00 00:00:00', '2020-02-29 16:17:28'),
('18', 'Lampung', 'Bandar Lampung', 'Arinal Djunaedi', '35.376 km²', '1964-03-18', 'Lampung adalah sebuah provinsi paling selatan di Pulau Sumatra, Indonesia, dengan ibu kota Bandar Lampung. Provinsi ini memiliki dua kota yaitu Kota Bandar Lampung dan Kota Metro serta 13 kabupaten', 'http://www.lampungprov.go.id', '/static/dist/img/lampung logo.png', '/static/dist/img/tari-sigeh-pengunten.png', 'Tari sigeh pengunten', '/static/dist/img/gamolan-lampung-1.jpg', 'gamolan Lampung', '/static/dist/img/Pakaian-Adat-Lampung.jpg', 'Pakaian Adat Lampung', '0000-00-00 00:00:00', '2020-02-29 16:28:50'),
('19', 'Kepulauan Bangka Belitung', NULL, NULL, NULL, NULL, NULL, NULL, '/static/dist/img/1200px-Coat_of_arms_of_Aceh.png', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('21', 'Kepulauan Riau', NULL, NULL, NULL, NULL, NULL, NULL, '/static/dist/img/1200px-Coat_of_arms_of_Aceh.png', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('31', 'DKI Jakarta', NULL, NULL, NULL, NULL, NULL, NULL, '/static/dist/img/1200px-Coat_of_arms_of_Aceh.png', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('32', 'Jawa Barat', '/static/dist/img/1200px-Coat_of_arms_of_Aceh.png', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('33', 'Jawa Tengah', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('34', 'DI Yogyakarta', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('35', 'Jawa Timur', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('36', 'Banten', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('51', 'Bali', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('52', 'Nusa Tenggara Barat', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('53', 'Nusa Tenggara Timur', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('61', 'Kalimantan Barat', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('62', 'Kalimantan Tengah', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('63', 'Kalimantan Selatan', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('64', 'Kalimantan Timur', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('65', 'Kalimantan Utara', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('71', 'Sulawesi Utara', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('72', 'Sulawesi Tengah', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('73', 'Sulawesi Selatan', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('74', 'Sulawesi Tenggara', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('75', 'Gorontalo', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('76', 'Sulawesi Barat', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('81', 'Maluku', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('82', 'Maluku Utara', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('91', 'Papua Barat', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('92', 'Papua', NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portal_code` int(11) NOT NULL,
  `ekspedisi` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`ekspedisi`)),
  `detail_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `via` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('0','1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL,
  `bukti_pembayaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `code`, `user_id`, `product_id`, `supplier_id`, `qty`, `subtotal`, `name`, `address`, `portal_code`, `ekspedisi`, `detail_address`, `via`, `resi`, `status`, `bukti_pembayaran`, `catatan`, `created_at`, `updated_at`) VALUES
(34, '20022806199', 9, 17, 2, 3, 150000, 'nanda', '171', 41311, '{\"code\":\"pos\",\"name\":\"POS Indonesia (POS)\",\"value\":7000,\"etd\":\"1 HARI\"}', 'Jalan Kepo Amat', 'MANDIRI', '213221431241221', '3', 'static/dist/img/Screenshot_20180116-134309.png', '', '2020-02-28 11:19:13', '2020-02-28 11:23:22'),
(35, '20022811419', 9, 18, 2, 3, 300000, 'Ray Nanda Pamungkas', '22', 40311, '{\"code\":\"jne\",\"name\":\"Jalur Nugraha Ekakurir (JNE)\",\"value\":8000,\"etd\":\"2-3\"}', 'dago no45', 'BCA', '1231234124124', '3', 'static/dist/img/Screenshot_20180116-134309.png', '', '2020-02-28 16:41:22', '2020-02-28 16:42:44'),
(37, '20022811529', 9, 17, 2, 1, 50000, 'Ray Nanda Pamungkas', '171', 41311, '{\"code\":\"jne\",\"name\":\"Jalur Nugraha Ekakurir (JNE)\",\"value\":8000,\"etd\":\"1-2\"}', 'jatirasa barat no.45', 'BCA', NULL, '0', NULL, '', '2020-02-28 16:52:36', '2020-02-28 16:52:39'),
(38, '20022912029', 9, 17, 2, 7, 350000, 'Ray Nanda Pamungkas', '171', 41311, '{\"code\":\"jne\",\"name\":\"Jalur Nugraha Ekakurir (JNE)\",\"value\":8000,\"etd\":\"1-2\"}', 'jatirasa barat 45', NULL, NULL, '1', 'static/dist/img/Screenshot_20180116-134309.png', '', '2020-02-28 17:02:34', '2020-02-28 17:03:36'),
(39, '20022912029', 9, 18, 2, 1, 100000, 'Ray Nanda Pamungkas', '171', 41311, '{\"code\":\"jne\",\"name\":\"Jalur Nugraha Ekakurir (JNE)\",\"value\":8000,\"etd\":\"1-2\"}', 'jatirasa barat 45', 'BCA', NULL, '0', NULL, '', '2020-02-28 17:02:35', '2020-02-28 17:02:38'),
(40, '20022904319', 9, 18, 2, 1, 100000, 'Ray Nanda Pamungkas', '171', 41311, '{\"code\":\"jne\",\"name\":\"Jalur Nugraha Ekakurir (JNE)\",\"value\":8000,\"etd\":\"1-2\"}', 'karawang barat,jatirasa barat no.45', 'MANDIRI', NULL, '0', NULL, '', '2020-02-28 21:31:12', '2020-02-28 21:31:16'),
(41, '20022904389', 9, 17, 2, 5, 250000, 'Ray Nanda Pamungkas', '16', 99777, '{\"code\":\"jne\",\"name\":\"Jalur Nugraha Ekakurir (JNE)\",\"value\":149000,\"etd\":\"9-10\"}', 'lorem', 'MANDIRI', NULL, '1', 'static/dist/img/Screenshot_20180116-134309.png', 'tes', '2020-02-28 21:38:40', '2020-02-28 21:40:41'),
(42, '20022905379', 9, 21, 2, 1, 250000, 'Ray Nanda Pamungkas', '171', 41311, '{\"code\":\"jne\",\"name\":\"Jalur Nugraha Ekakurir (JNE)\",\"value\":8000,\"etd\":\"1-2\"}', 'jatirasa barat no.45', 'BCA', '1231234124124', '3', 'static/dist/img/Screenshot_20180116-134309.png', NULL, '2020-02-28 22:37:08', '2020-02-28 22:39:29'),
(43, '20022907409', 9, 21, 2, 1, 250000, 'Ray Nanda Pamungkas', '171', 41311, '{\"code\":\"jne\",\"name\":\"Jalur Nugraha Ekakurir (JNE)\",\"value\":8000,\"etd\":\"1-2\"}', 'jatirasa bsra no45', 'BCA', '1231234124124', '2', 'static/dist/img/Screenshot_20180116-134309.png', NULL, '2020-02-29 00:40:53', '2020-02-29 00:42:38'),
(44, '20022901519', 9, 21, 2, 1, 250000, 'Ray Nanda Pamungkas', '5', 23719, '{\"code\":\"jne\",\"name\":\"Jalur Nugraha Ekakurir (JNE)\",\"value\":63000,\"etd\":\"3-6\"}', 'zz', 'MANDIRI', NULL, '0', NULL, NULL, '2020-02-29 06:51:53', '2020-02-29 06:52:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '/static/dist/img/avatar5.png',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('L','P') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `birthday` date NOT NULL,
  `role` enum('admin','supplier','member') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `photo`, `username`, `email`, `password`, `address`, `phone`, `gender`, `status`, `birthday`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ray Nanda Pamungkas', '/static/dist/img/avatar5.png', 'raynanda123', 'pamungkasray229@gmail.com', '$2y$10$QFjC75CmquWO/i.zvIaUu.e8Fo4VVBGDpGh8WqA1aYRijyvXacHom', 'jatirasa barat', '083815854063', 'L', '1', '2020-02-08', 'admin', NULL, '2020-02-07 15:57:47', '2020-02-07 15:57:47'),
(2, 'deby amaliaaaa', 'static/dist/img/IMG_0012.JPG', 'debyamalia123', 'deby@gmail.com', '$2y$10$8m.oTclFrACYpQunm66NI.dFXG79QAOfFoFdoI4VAaT2JKHK8mGDa', 'Karawang', '0989878', 'L', '1', '2020-02-15', 'supplier', NULL, '2020-02-07 16:24:23', '2020-02-12 08:47:00'),
(9, 'pamungkass', '/static/dist/img/avatar5.png', 'pamungkas', 'if18.raypamungkas@mhs.ubpkarawang.ac.id', '$2y$10$751O4Dxz2wbpG4Gd8Pb5g.i.R0q.SrXfqWxJZR/5GKvKLLbkZ3B7q', 'Karawang', '083815854063', 'L', '1', '2020-01-26', 'member', 'Hhy6f7vF3bgIzEWyp00WHpNbHVZ9uvaM9YbtShXfuvZjcHgfU09XtaasrPNd', '2020-02-11 15:30:38', '2020-02-28 16:19:37'),
(10, 'andri', '/static/dist/img/avatar5.png', 'andri', 'andri@gmail.com', '$2y$10$p3sXsR0.E0WaDNU37gmfA.pO3i5V2srjnlk59aaBF6P6eJGV0Fqc6', 'Karawang', '01011', 'L', '0', '2020-02-13', 'supplier', 'YW5kcmlAZ21haWwuY29t', '2020-02-13 00:18:19', '2020-02-13 00:18:19'),
(11, 'bagas', '/static/dist/img/avatar5.png', 'bagas', 'bagas@gmail.com', '$2y$10$.h6L2XukuRhSiTd7nbe9.OX5w0RM4QHBRBGSgny09pdv/szyPWZqq', 'Karawang', '9099090', 'L', '0', '2020-02-11', 'member', 'YmFnYXNAZ21haWwuY29t', '2020-02-13 00:19:24', '2020-02-13 00:19:24'),
(12, 'lorem', '/static/dist/img/avatar5.png', 'lorem', 'lorem@gmail.com', '$2y$10$1rOhWYyB6sX70XYeoyHSuuXYcwbT8LLJXU4AQPhmqZs8NTomYX6sG', 'Karawang', '9890890', 'P', '0', '2020-02-07', 'member', 'bG9yZW1AZ21haWwuY29t', '2020-02-13 00:26:32', '2020-02-13 00:26:32'),
(13, 'rusdi firdaus', '/static/dist/img/avatar5.png', 'rusdi', 'rusdimalik99@gmail.com', '$2y$10$KliE5rfsqjvbjxCC8SvdzeOjEYvYLSSahohr.MpKVavWx7gllTZrO', 'Karawang', '083815854063', 'L', '1', '2020-02-03', 'supplier', 'HdUSYAQmBtpE9PX8BoAcKELiTTN7oYnfuCt5FaYJpGzy99doYAJyM8yjSBdB', '2020-02-26 21:54:09', '2020-02-26 21:59:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`),
  ADD KEY `categories_user_id_foreign` (`user_id`);

--
-- Indexes for table `keluhans`
--
ALTER TABLE `keluhans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`),
  ADD KEY `products_user_id_foreign` (`user_id`);

--
-- Indexes for table `provinsis`
--
ALTER TABLE `provinsis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_user_id_foreign` (`user_id`),
  ADD KEY `transactions_product_id_foreign` (`product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `keluhans`
--
ALTER TABLE `keluhans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `categories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
